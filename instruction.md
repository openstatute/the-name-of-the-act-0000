---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, 
      https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---
# How to write in Markdown

>Disclaimer:
Although every possible care has been taken to prepare the Acts and Rules published in the OpenStatute Initiative,Contributor or any of its employees is not responsible for any mistake or in accuracy that might have crept in or any loss or damage resulting out of such unintended mistakes. Endusers are advised to verify and check the contents with the Official Gazettes or other means. Any mistake or inaccuracy reported will be highly appreciated and duly incorporated in subsequent editions.

# The Name of The Act 0000 

<embed src="../Notifications_Log.md"/>

<table>
  <tc>
    <th>Act ID</th>
    <th>Act No</th>
    <th>Act Year</th>
    <th>Enactment Date</th>
    <th>Enforcement Date<sup><abbr title="Vide Notification No.L.3067, dated 24th February 1937, published in the Gazette of India, 1937, Pt I, p.303.">1</abbr></sup></th>
  </tc>
  <tr>
    <td>000000</td>
    <td>00</td>
    <td>00</td>
    <td>00</td>
    <td>00</td>
  </tr>
</table>

<table>
    <tr>
    <th>Ministry</th>
    <td>00</td>
  </tr>
    <tr>
    <th>Short Title</th>
    <td>00</td>
  </tr>
    <tr>
    <th>Long Title</th>
    <td>00</td>
  </tr>
</table>

1. Vide Notification No.L.3067, dated 24th February 1937, published in the Gazette of India, 1937, Pt I, p.303.


1
 Vide G.S.R 2367dated 21st August, 1975, published in the Gazette of India, Part-II Sec. 3(i), dated 6th September, 1975.

2 Ins. By S.O. 251, dated 7th January, 1984 (w.e.f 21-1-1984)

2 Ins by G.S.R 1147 dated 23rd August, 1979 ( w.e.f 8-9-1979) 

1. Ins. by s. 4, ibid. (w.e.f. 9-11-2005).
2. Subs. by Act 53 of 1964, s. 3, for clause (i) (w.e.f. 1-2-1965).
3. Clauses (i), (ia) and (ib) renumbered as clauses (ia), (ib) and (ic) thereof by Act 41 of 2005, s. 4 (w.e.f. 9-11-2005).
4. Subs. by Act 38 of 1982, s. 4, for "industrial establishment" means" (w.e.f. 15-10-1982).
5. Subs. by Act 53 of 1964, s. 3, for sub-clause (a) (w.e.f. 1-2-1965).





<abbr title="Text">Abbrevation_text</abbr>

<code>Text</code>

<q>Text</q>

<p>text <sub> text</sub></p>
<p>text <sub> text</sub>

<table align="center" cellpadding = "4">
<tr>
<td>FIRST NAME</td>
<td><input type="text" name="First_Name"/>
</td>
</tr>
<tr>
<td>LAST NAME</td>
<td><input type="text" name="Last_Name"/>
</td>
</tr>
<tr>
<td>Employee ID</td>
<td><input type="text" name="empid" />
</td>
</tr>
<tr>
<td>DATE OF BIRTH</td>
<td><input type="date" name="Birth_date" /></td>
</tr>
<tr>
<td>EMAIL ID</td>
<td><input type="text" name="Email_Id" maxlength="100" /></td>
</tr>
<tr>
<td>MOBILE NUMBER</td>
<td>
<input type="text" name="Mobile_Number" maxlength="10" />
</td>
</tr>
<tr>
<td>GENDER</td>
<td>
Male 
</td>
</tr>
<tr>
<td>CITY</td>
<td><input type="text" name="City" maxlength="20" />
</td>
</tr>
<tr>
<td>Department</td>
<td><input type="text" name="dept"  />
</td>
</tr>
<tr>
<td colspan="2" align="center">
<input type="submit" value="Submit">
<input type="reset" value="Reset">
</td>
</tr>
</table>


